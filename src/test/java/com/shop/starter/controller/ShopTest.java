package com.shop.starter.controller;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.shop.starter.model.Item;

public class ShopTest {
	private Storage storage;
	private Shop shop;
	
	
	@Before
	public void init() {
		storage=new Storage();
		shop=new Shop(storage,1000);
		
		Item a1=new Item("Cup", 1, 10, 50);
		storage.addItem(a1);
	}
	
	@Test
	public void sellItemCheckCash() {
		shop.sell("Cup");
		assertTrue(shop.getCash() == 1010);
		
	}
	
	@Test
	public void sellItemCheckNb() {	
		shop.sell("Cup");
		assertTrue(storage.getItem("Cup").getNbrElt() == 49);
	}

}
