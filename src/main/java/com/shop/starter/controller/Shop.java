package com.shop.starter.controller;

import com.shop.starter.model.Item;

public class Shop {
	
	Storage storage ;
	float cash;
	Item item;
	
	public Item sell(String name) {
		Item item = storage.getItem(name);
		cash=cash+item.getPrice();
		storage.removeItem(name);
		return item;
	}
	
	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public float getCash() {
		return cash;
	}
	public void setCash(float cash) {
		this.cash = cash;
	}
	@Override
	public String toString() {
		return "Shop [storage=" + storage + ", cash=" + cash + "]";
	}
	public Shop(Storage storage, float cash) {
		super();
		this.storage = storage;
		this.cash = cash;
	}
	public Shop() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
