package com.shop.starter.controller;

import java.util.HashMap;
import java.util.Map;

import com.shop.starter.model.Item;

public class Storage {
	private Map<String , Item> itemMap;
	
	public Storage() {
		itemMap=new HashMap<String, Item>();
	}
	
	public void addItem(Item obj) {
		this.itemMap.put(obj.getName(), obj);
	}
	public Item getItem(String name) {
		Item obj;
		obj = this.itemMap.get(name);
		return obj;
	}
	
	public boolean removeItem(String name) {
		Item it= this.getItem(name);
		if(it.getNbrElt()-1>0) {
			it.setNbrElt(it.getNbrElt()-1);
			return true;
		}else {
			if( this.itemMap.remove(name)!=null) {
				return true;
			}
			return false;
		}
	}
}