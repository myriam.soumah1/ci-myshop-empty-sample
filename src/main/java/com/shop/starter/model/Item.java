package com.shop.starter.model;

public class Item {
	
	private String name;
	private int id;
	private float price;
	private 
	int nbrElt;
	
	public Item() {
		super();
	}
	
	public Item(String name, int id, float price, int nbrElt) {
		super();
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public float getPrice() {
		return price;
	}


	public void setPrice(float price) {
		this.price = price;
	}


	public int getNbrElt() {
		return nbrElt;
	}


	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
	
	

	
	public String display() {
		
		return "Item [ name = "+ name +", id = "+id+", price = "+price+", nbrElt = "+nbrElt+"]";

}
}